<?php 
namespace App\Astrology;

use Illuminate\Database\Eloquent\Model;
use App\Astrology\Sign;

class House extends Model{
    protected $appends = ['sign', 'house_no', 'house_array'];

    const HOUSES = [
        0 => [
            'name' => 'first',
            'abvr' => '1st',
        ],
        1 => [
            'name' => 'second',
            'abvr' => '2nd',
        ],
        2 => [
            'name' => 'third',
            'abvr' => '3rd',
        ],
        3 => [
            'name' => 'forth',
            'abvr' => '4th',
        ],
        4 => [
            'name' => 'fifth',
            'abvr' => '5th',
        ],
        5 => [
            'name' => 'sixth',
            'abvr' => '6th',
        ],
        6 => [
            'name' => 'seventh',
            'abvr' => '7th',
        ],
        7 => [
            'name' => 'eighth',
            'abvr' => '8th',
        ],
        8 => [
            'name' => 'nineth',
            'abvr' => '9th',
        ],
        9 => [
            'name' => 'tenth',
            'abvr' => '10th',
        ],
        10 => [
            'name' => 'eleventh',
            'abvr' => '11th',
        ],
        11 => [
            'name' => 'twelfth',
            'abvr' => '2nd',
        ],
    ];
    const PLACIDUS_RANGES = [
        
    ];
    public $index;
    public $long;
    public function __construct($index, $long)
    {
        $this->index = $index;
        $this->long = $long;
    }

    public function getSignAttribute()
    {
        return Sign::decodeSign($this->long);
    }

    public function getHouseNoAttribute()
    {
        return self::HOUSES[$this->index]['name'];
    }
    public function getHouseArrayAttribute()
    {
        return self::HOUSES[$this->index];
    }
}

?>