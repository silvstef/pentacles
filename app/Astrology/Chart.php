<?php 

namespace App\Astrology;
use App\Astrology\HeavenlyBodies\Sun;
use App\Astrology\HeavenlyBodies\Mercury;
use App\Astrology\HeavenlyBodies\Venus;
use App\Astrology\HeavenlyBodies\Mars;
use App\Astrology\HeavenlyBodies\Jupiter;
use App\Astrology\HeavenlyBodies\Saturn;
use App\Astrology\HeavenlyBodies\Uranus;
use App\Astrology\HeavenlyBodies\Neptune;
use App\Astrology\HeavenlyBodies\Pluto;
use App\Astrology\Moon\Moon;
use App\Astrology\Moon\NorthNode;
use App\Astrology\Moon\SouthNode;
use App\Astrology\Helpers\Convert;
use App\Astrology\Sign;
use App\Astrology\Houses;
use Illuminate\Database\Eloquent\Model;

class Chart extends Model{

    public $day;
    public $month;
    public $year;
    public $gmt;
    public $hour;
    public $minute;
    private $unkown_time = false;

    public $lat;
    public $long;

    public $planets;
    public $houses = null;
    public function __construct($day, $month, $year, $gmt, $lat, $long, $hour = null, $minute = null)
    {
        $this->day = $day;
        $this->month = $month;
        $this->year = $year;
        $this->gmt = $gmt;
        $this->lat = $lat;
        $this->long = $long;
        if($hour === null && $minute === null)
        {
            $this->unkown_time = true;
        }
        else
        {
            $this->hour = $hour;
            $this->minute = $minute;
        }
        
        if(!$this->unkown_time)
        {
            $this->assignHousesAsAttributes();
        }

        $this->assignPlanetsAsAttributes();

    }

    public function assignPlanetsAsAttributes()
    {
        $planets = self::getPlanets($this->day, $this->month, $this->year, $this->gmt, true);
        $i = 0;
        collect($planets)->each(function($heavenly_body, $name) use($i){
            if(!$this->unkown_time)
            {
                $planet_house = Sign::decodeSign(($heavenly_body->long - $heavenly_body->dcl + $this->houses[House::HOUSES[$i]['name']]['long']));
                $test = [
                    //'0' => $heavenly_body,
                    'test2' => Sign::decodeSign(($heavenly_body->long - $heavenly_body->dcl)),
                    //'test3' => Sign::decodeSign(($heavenly_body->long - $heavenly_body->c)),
                    'test4' => Sign::decodeSign(($heavenly_body->long - $this->houses['first']['long'])),
                    //'test3' => Sign::decodeSign(Convert::Mod360($heavenly_body->lat)),
                    //'test4' => Sign::decodeSign(Convert::Mod2pi($heavenly_body->lat)),
                    //'test5' => Sign::decodeSign(Convert::Mod2pi($heavenly_body->dcl)),
                ];
            }
            else
            {
                $planet_house = null;
            }
            $this->attributes['planets'][$name] = [
                'sign' => Sign::decodeSign($heavenly_body->long, $heavenly_body),
                'planet_house' => $planet_house,
                'tests' => $test,
            ];
            $i++;
        });
    }

    public function assignHousesAsAttributes()
    {
        $houses = new Houses($this->day, $this->month, $this->year, $this->gmt, $this->lat, $this->long);
        $this->attributes['extra']['ascendent'] = Sign::decodeSign($houses->asc);
        $this->attributes['extra']['midheaven'] = Sign::decodeSign($houses->mc);
        $this->attributes['extra']['east_point'] = Sign::decodeSign($houses->ep);
        $this->attributes['extra']['vx'] = Sign::decodeSign($houses->vx);
        collect($houses->house)->each(function($long, $name){
            $house = new House($name, $long);
            $this->attributes['houses'][$name] = $house->toArray();
        });
    }

    static public function getPlanets($day, $month, $year, $gmt, $indexed = false)
    {
        $sun = new Sun($day, $month, $year, $gmt);
        $mercury = new Mercury($sun, $day, $month, $year, $gmt);
        $venus = new Venus($sun, $day, $month, $year, $gmt);
        $mars = new Mars($sun, $day, $month, $year, $gmt);
        $jupiter = new Jupiter($sun, $day, $month, $year, $gmt);
        $saturn = new Saturn($sun, $day, $month, $year, $gmt);
        $uranus = new Uranus($sun, $day, $month, $year, $gmt);
        $neptune = new Neptune($sun, $day, $month, $year, $gmt);
        $pluto = new Pluto($sun, $day, $month, $year, $gmt);

        $moon = new Moon($day, $month, $year, $gmt);
        $north_node = new NorthNode($moon);
        $south_node = new SouthNode($north_node);

        if($indexed)
        {
            return [
                0 => $sun,
                1 => $moon,
                2 => $mercury,
                3 => $venus,
                4 => $mars,
                5 => $jupiter,
                6 => $saturn,
                7 => $uranus,
                8 => $neptune,
                9 => $pluto,
                10 => $south_node,
                11 => $north_node,
            ];
        }
        return [
            'sun' => $sun,
            'moon' => $moon,
            'mercury' => $mercury,
            'venus' => $venus,
            'mars' => $mars,
            'jupiter' => $jupiter,
            'saturn' => $saturn,
            'uranus' => $uranus,
            'neptune' => $neptune,
            'pluto' => $pluto,
            'south_node' => $south_node,
            'north_node' => $north_node,
        ];
    }

    public function getAscAttribute()
    {
        return $this->ascendent;
    }
    
    public function getMcAttribute()
    {
        return $this->midheaven;
    }
    
    public function getEpAttribute()
    {
        return $this->east_point;
    }
    
}

?>