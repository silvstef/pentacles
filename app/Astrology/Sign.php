<?php 

namespace App\Astrology;

use Illuminate\Database\Eloquent\Model;
use App\Astrology\Helpers\Convert;

class Sign extends Model
{
    const SIGNS = [
        'aries',
        'taurus',
        'gemini',
        'cancer',
        'leo',
        'virgo',
        'libra',
        'scorpio',
        'sagittarius',
        'capricorn',
        'aquarius',
        'pisces',
    ];

    const FIRE = [0,4,8];
    const EARTH = [1,5,9];
    const AIR = [2,6,10];
    const WATER = [3,7,11];
    const ELEMENTS = ['fire', 'earth', 'air', 'water'];
    const ELEMENTS_EXTENDED = [
        'fire' => [
            0 => 'cardinal',
            4 => 'fixed',
            8 => 'mutable'
        ],
        'earth' => [
            1 => 'fixed',
            5 => 'mutable',
            9 => 'cardinal'
        ],
        'air' => [
            2 => 'mutable',
            6 => 'cardinal',
            10 => 'fixed',
        ],
        'water' => [
            3 => 'cardinal',
            7 => 'fixed',
            11 => 'mutable',
        ],
    ];
    const ZODIAC_EXTENDED = [
        0 => [
            'element' => 'fire',
            'quality' => 'cardinal',
        ],
        1 => [
            'element' => 'earth',
            'quality' => 'fixed',
        ],
        2 => [
            'element' => 'air',
            'quality' => 'mutable',
        ],
        3 => [
            'element' => 'water',
            'quality' => 'cardinal',
        ],
        4 => [
            'element' => 'fire',
            'quality' => 'fixed',
        ],
        5 => [
            'element' => 'earth',
            'quality' => 'mutable',
        ],
        6 => [
            'element' => 'air',
            'quality' => 'cardinal',
        ],
        7 => [
            'element' => 'water',
            'quality' => 'fixed',
        ],
        8 => [
            'element' => 'fire',
            'quality' => 'mutable',
        ],
        9 => [
            'element' => 'earth',
            'quality' => 'cardinal',
        ],
        10 => [
            'element' => 'air',
            'quality' => 'fixed',
        ],
        11 => [
            'element' => 'water',
            'quality' => 'mutable',
        ],
    ];
    
    protected $appends = ['index','name','abvr','house','element','quality','planet', 'planet_name'];
    protected $hidden = ['heavenly_body'];
    public $index;
    public $minutes;
    public $seconds;
    public $heavenly_body;
    public function __construct($index, $minutes, $seconds, $heavenly_body = null)
    {
        $this->index = $index;
        $this->minutes = $minutes;
        $this->seconds = $seconds;
        $this->heavenly_body = $heavenly_body;
    }
    
    public function getIndexAttribute()
    {
        return $this->index;
    }

    public function getNameAttribute()
    {
        return ucfirst(self::SIGNS[$this->index]);
    }

    public function getHouseAttribute()
    {
        return $this->index + 1;
    }

    public function getAbvrAttribute()
    {
        return strtoupper(substr(self::SIGNS[$this->index],0,3));
    }

    public function getElementAttribute()
    {
        return self::ZODIAC_EXTENDED[$this->index]['element'];
    }
    
    public function getQualityAttribute()
    {
        return self::ZODIAC_EXTENDED[$this->index]['quality'];
    }

    public function getPlanetAttribute()
    {
        return $this->heavenly_body;
    }
    
    public function getPlanetNameAttribute()
    {
        if($this->heavenly_body === null)
        {
            return null;
        }
        return $this->heavenly_body->name;
    }

    static public function decodeSign($long, $heavenly_body = null)
    {
        $convert = Convert::DecToZodGlyph($long);
        $sign = new Sign($convert['s'], $convert['d'], $convert['m'], $heavenly_body);
        return $sign;
    }
}

?>