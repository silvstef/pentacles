<?php

namespace App\Astrology;

use App\Astrology\Helpers\JulianDate;
use App\Astrology\Helpers\Convert;
use App\Astrology\Helpers\Coordinates;

class Planet
{
	private $julianDate;
	private $mean_anomaly;
	private $eccentricity;
	private $perihelion_argument;
	private $ascending_node;
	private $inclination;
	private $harmonics;
	private $astronomical_unit;
	private $index;
	
	private $tx;
	private $no_terms;
	
	private $ci;
	private $ci2;
	
	private $coord;
	private $eccentric_angle;
	private $ellipse_perimeter;
	private $ellipse_point1;
	private $mean_radial;
	private $perihelion_angle;
	private $inclination_angle;
	public $asc_node_angle;

	public $xw;
	public $yw;
	public $xh;
	public $yh;

	public $xx;
	public $yy;
	public $zz;

	public $angle;
	public $rx;
	public $long;
	public $lat;
	public $dcl;

	public $sun;

	public function __construct($sun, $mean_anomaly, $eccentricity, $perihelion_argument, $ascending_node, $inclination, $harmonics, $no_term, $astronomical_unit, JulianDate $julianDate)
	{
		$this->sun = $sun;
		$this->mean_anomaly = $mean_anomaly;
		$this->eccentricity = $eccentricity;
		$this->perihelion_argument = $perihelion_argument;
		$this->ascending_node = $ascending_node;
		$this->inclination = $inclination;
		$this->harmonics = $harmonics;
		$this->astronomical_unit = $astronomical_unit;

		$this->ci = $julianDate->ci;
		$this->ci2 = $julianDate->ci * $julianDate->ci;

		$this->coord = new Coordinates();
	}

	public function initialCalculations()
	{
		$this->eccentric_angle = $this->mean_radial = deg2rad(
			Convert::Mod360(
				$this->mean_anomaly[0] + $this->mean_anomaly[1] * $this->ci + $this->mean_anomaly[2] * $this->ci2
			)
		);
		$this->ellipse_perimeter = $this->eccentricity[0] + $this->eccentricity[1] * $this->ci + $this->eccentricity[2] * $this->ci2;
		$this->perihelion_angle = deg2rad($this->perihelion_argument[0] + $this->perihelion_argument[1] * $this->ci + $this->perihelion_argument[2] * $this->ci2);
		$this->asc_node_angle = deg2rad($this->ascending_node[0] + $this->ascending_node[1] * $this->ci + $this->ascending_node[2] * $this->ci2);
		$this->inclination_angle = deg2rad($this->inclination[0] + $this->inclination[1] * $this->ci + $this->inclination[2] * $this->ci2);
		for ($j=1; $j<7; $j++)
		{
			$this->eccentric_angle = $this->mean_radial + $this->ellipse_perimeter * sin($this->eccentric_angle);
		}
		$this->ellipse_point1 = .01720209 / (pow($this->astronomical_unit,1.5) * (1 - $this->ellipse_perimeter * cos($this->eccentric_angle)));
	}

	public function calculatePointsOfOrigin()
	{
		$this->xw = -($this->astronomical_unit * $this->ellipse_point1) * sin($this->eccentric_angle);
		$this->yw = ($this->astronomical_unit * $this->ellipse_point1) * sqrt(1 - $this->ellipse_perimeter * $this->ellipse_perimeter ) * cos($this->eccentric_angle);
		$this->coord->RecToPol($this->xw, $this->yw);	
		$this->coord->PolToRec($this->coord->a + $this->perihelion_angle, $this->coord->r);
		$d = $this->coord->x;
		$this->coord->RecToPol($this->coord->y, 0);
	
		$this->coord->PolToRec($this->coord->a + $this->inclination_angle, $this->coord->r);
		
		$this->coord->RecToPol($d, $this->coord->x);
		$angle = $this->coord->a;
		$angle += $this->asc_node_angle;
		if ($angle < 0)
		{
			$angle += (2 * M_PI);
		}
		$this->coord->PolToRec($angle, $this->coord->r);
	
		$this->xh = $this->coord->x;
		$this->yh = $this->coord->y;
	
		$x = $this->astronomical_unit * (cos($this->eccentric_angle) - $this->ellipse_perimeter);
		$y = $this->astronomical_unit * sin($this->eccentric_angle) * sqrt(1 - $this->ellipse_perimeter * $this->ellipse_perimeter);
	
		$this->coord->RecToPol($x, $y);
		$this->coord->PolToRec($this->coord->a + $this->perihelion_angle, $this->coord->r);
		$d = $this->coord->x;
		$this->coord->RecToPol($this->coord->y, 0);
		$this->coord->PolToRec($this->coord->a + $this->inclination_angle, $this->coord->r);
		$g = $this->coord->y;
		$this->coord->RecToPol($d, $this->coord->x);
		$angle = $this->coord->a;
		$angle += $this->asc_node_angle;
		if ($angle < 0)
		{
			$angle += (2 * M_PI);
		}
		$this->coord->PolToRec($angle, $this->coord->r);
		$this->xx = $this->coord->x;
		$this->yy = $this->coord->y;
		$this->zz = $g;
	}


	public function calculateHarmonics()
	{
		if($this->harmonics !== null)
		{
			$tx = [0,0,0];
			for($i = 0; $i < 3; $i++)
			{
				if($i == 2)
				{
					$this->no_terms--;
				}
				$hi = 0;
				$s = deg2rad($this->harmonics[$hi] + $this->harmonics[$hi+1] * $this->ci + $this->harmonics[$hi+2] * $this->ci2);
				$hi +=3;
				$a = 0;
				for($j = 0; $j < $this->no_terms;$j++)
				{
					$u1 = $this->harmonics[$hi++];
					$v = $this->harmonics[$hi++];
					$w = $this->harmonics[$hi++];
					$a = $a + deg2rad($u1) * cos(($v * $ci + $w) * M_PI/180);
				}
				$tx[$i] = rad2deg($s + $a);
				if($i == 2 && $this->name == 'Jupiter')
				{
					$tx[2] = 0;
				}
			}
			$this->xx += $tx[1];
			$this->yy += $tx[0];
			$this->zz += $tx[2];
		}
	}

	public function calculateOrbit()
	{
		if($this->sun != null)
		{
			$this->xx -= $this->sun->xx;
			$this->yy -= $this->sun->yy;
			$this->zz -= $this->sun->zz;
			$this->xw = $this->xh - $this->sun->xh;
			$this->yw = $this->yh - $this->sun->yh;
			$xk = ($this->xx * $this->yw - $this->yy * $this->xw) / ($this->xx * $this->xx + $this->yy * $this->yy);
		}
		else
		{
			$xk = ($this->xx * $this->yh - $this->yy * $this->xh) / ($this->xx * $this->xx + $this->yy * $this->yy);
		}
		
		$this->coord->RecToPol($this->xx, $this->yy);
		$this->c = Convert::Mod360(rad2deg($this->coord->a));
		$this->coord->RecToPol($this->coord->r, $this->zz);

		$br = .0057756 * sqrt($this->xx * $this->xx + $this->yy * $this->yy + $this->zz * $this->zz) * rad2deg($xk);
		
		$this->coord->RecToPol($this->xx, $this->yy);
		$c = rad2deg($this->coord->a) - $br;

		$this->coord->RecToPol($this->coord->r, $this->zz);
		$this->angle = $this->coord->a;
		if ($this->angle > .35)
		{
			$this->angle -= (2 * M_PI);
		}
		$this->lat = rad2deg($this->angle);
		$this->long = Convert::Mod360($c);

		$ob = deg2rad(23.452294 - .0130125 * $this->ci);
		$this->Declin($ob);
		if($this->sun === null)
		{
			$this->long = Convert::Mod360($c + 180);
		}
	}

	private function Declin($ob)
	{
		$coord = new Coordinates();
		$angle = deg2rad($this->lat);
		$radius = 1;
		$coord->PolToRec($angle, $radius);
		$q = $coord->y;
		$radius = $coord->x;
		$angle = deg2rad($this->long);
		$coord->PolToRec($angle, $radius);
		$x = $coord->y;
		$y = $q;
		$coord->RecToPol($x, $y);
		$angle = $coord->a + $ob;
		$coord->PolToRec($angle, $coord->r);
		$this->dcl = rad2deg(asin($coord->y));
		return $this->dcl;
	}

	static public function staticDeclin($ob, $long, $lat)
	{
		$coord = new Coordinates();
		$angle = deg2rad($lat);
		$radius = 1;
		$coord->PolToRec($angle, $radius);
		$q = $coord->y;
		$radius = $coord->x;
		$angle = deg2rad($long);
		$coord->PolToRec($angle, $radius);
		$x = $coord->y;
		$y = $q;
		$coord->RecToPol($x, $y);
		$angle = $coord->a + $ob;
		$coord->PolToRec($angle, $coord->r);
		$dcl = rad2deg(asin($coord->y));
		return $dcl;
	}
}
