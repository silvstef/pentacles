<?php
namespace App\Astrology\Moon;

use App\Astrology\Interfaces\HeavenlyBody;
use App\Astrology\Helpers\Convert;
use App\Astrology\Planet;

class SouthNode implements HeavenlyBody
{
    public $index = 11;
    public $name = 'South Node';
    public $rx;
	public $long;
	public $lat;
    public $dcl;
    
    public $north_node;
    public $moon;
    public function __construct(NorthNode $north_node)
    {
        $this->name = 'South Node';
        $this->north_node = $north_node;
        $this->moon = $this->north_node->moon;

        $this->long = Convert::Mod360($this->north_node->long+180);
		$this->rx = $this->north_node->rx;
        $this->lat = 0.0;
        
        $this->dcl = Planet::staticDeclin($this->moon->ob, $this->long, $this->lat);
    }

}

?>