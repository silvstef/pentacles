<?php
namespace App\Astrology\Moon;

use App\Astrology\Interfaces\HeavenlyBody;
use App\Astrology\Helpers\Convert;
use App\Astrology\Planet;

class NorthNode implements HeavenlyBody
{
    public $index = 10;
    public $name = 'North Node';
    public $rx;
	public $long;
	public $lat;
    public $dcl;
    
    public $moon;
    public function __construct(Moon $moon)
    {
        $this->name = 'North Node';
        $this->moon = $moon;
        $f = $this->moon->f;
        $y = $this->moon->y;
        $n = $this->moon->n;
        $l1 = $this->moon->l1;
        $l = $this->moon->l;
        $m = $this->moon->m;

        $tn = $n + 5392.0 * sin(deg2rad(2 * $f - $y)) - 541.0 * sin(deg2rad($l1)) - 442.0 * sin(deg2rad($y));
		$tn = $tn + 423.0 * sin(deg2rad(2.0 * $f)) - 291.0 * sin(deg2rad(2.0 * $l - 2.0 * $f));

		$this->long = Convert::Mod360($tn / $m);
	
		if ($this->long < 0)
		{
			$this->long = abs($this->long);
			$this->rx = 'R';
		}
		else
		{
			$this->rx = ' ';
		}
	
		$this->lat = 0.0;
        
        $this->dcl = Planet::staticDeclin($this->moon->ob, $this->long, $this->lat);
    }

}

?>