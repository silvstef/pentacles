<?php 
namespace App\Astrology\Moon;

use App\Astrology\Interfaces\HeavenlyBody;
use App\Astrology\Helpers\JulianDate;
use App\Astrology\Helpers\Convert;
use App\Astrology\Planet;

class Moon implements HeavenlyBody
{
    public $index = 9;
    public $name = "Moon";
    private $julianDate;
    private $ci;
    private $ci2;

    public $mean_node_long;
    public $n;
    public $l;
    public $l1;
    public $f;
    public $y;
    public $d;
    public $ll;

    public $ml;
    public $mb;

    public $m;

    public $rx;
	public $long;
	public $lat;
    public $dcl;
    
    public $ob;

    public function __construct($d, $m, $y, $gmt)
    {
        $this->name = "Moon";
        $this->julianDate = new JulianDate($m, $d, $y, $gmt);
        $this->ci = $this->julianDate->ci;
        $this->ci2 = $this->ci * $this->ci;
        
        $this->m = 3600.0;

        $this->calculatePointsOfOrigin();

        $this->calculateMoonPerturbations();

        $this->calculateOrbit();
    }
    
    private function calculatePointsOfOrigin()
    {
        $this->ll = 973563.0 + 1732564379.0 * $this->ci - 4.0 * $this->ci2;
        $g = 1012395.0 + 6189.0 * $this->ci;
        $this->n = 933060.0 - 6962911.0 * $this->ci + 7.5 * $this->ci2;
        //Mean Node		
        $this->mean_node_long = fmod($this->n/$this->m, 360);
    
        $g1 = 1203586.0 + 14648523.0 * $this->ci - 37.0 * $this->ci2;
        $this->d = 1262655.0 + 1602961611.0 * $this->ci - 5.0 * $this->ci2;
        $this->l = ($this->ll - $g1) / $this->m;
        $this->l1 = (($this->ll - $this->d) - $g) / $this->m;
        $this->f = ($this->ll - $this->n) / $this->m;
        $this->d /= $this->m;
        $this->y = 2.0 * $this->d;

    }

    private function calculateMoonPerturbations()
    {
        $ml = 22639.6  * sin(deg2rad($this->l)) - 4586.4 * sin(deg2rad($this->l - $this->y));
        $ml = $ml + 2369.9 * sin(deg2rad($this->y)) + 769.0 * sin(deg2rad(2.0 * $this->l)) - 669.0 * sin(deg2rad($this->l1));
        $ml = $ml - 411.6 * sin(deg2rad(2 * $this->f)) - 212.0 * sin(deg2rad( 2.0 * $this->l - $this->y));
        $ml = $ml - 206.0 * sin(deg2rad($this->l + $this->l1 - $this->y)) + 192.0 * sin(deg2rad($this->l + $this->y));
        $ml = $ml - 165.0 * sin(deg2rad($this->l1 - $this->y)) + 148.0 * sin(deg2rad($this->l - $this->l1)) - 125.0 * sin(deg2rad($this->d));
        $ml = $ml - 110.0 * sin(deg2rad($this->l + $this->l1)) - 55.0 * sin(deg2rad(2.0 * $this->f - $this->y));
        $ml = $ml - 45.0 * sin(deg2rad($this->l + 2.0 * $this->f)) + 40.0 * sin(deg2rad($this->l - 2.0 * $this->f));
        $this->ml = $ml;
        $mb = 18461.5 * sin(deg2rad($this->f)) + 1010.0 * sin(deg2rad($this->l + $this->f)) - 999.0 * sin(deg2rad($this->f - $this->l));
		$mb = $mb - 624.0 * sin(deg2rad($this->f - $this->y)) + 199.0 * sin(deg2rad($this->f + $this->y - $this->l));
		$mb = $mb - 167.0 * sin(deg2rad($this->l + $this->f - $this->y)) + 117.0 * sin(deg2rad($this->f + $this->y));
		$mb = $mb + 62.0 * sin(deg2rad(2.0 * $this->l + $this->f)) - 33.0 * sin(deg2rad($this->f - $this->y - $this->l));
		$mb = $mb - 32.0 * sin(deg2rad($this->f - 2.0 * $this->l)) - 30.0 * sin(deg2rad($this->l1 + $this->f - $this->y));
		$this->mb = $mb;
    }
    private function calculateOrbit()
    {
        $this->ob = deg2rad(23.452294 - .0130125 * $this->ci);
		$this->long = Convert::Mod360(($this->ll + $this->ml) / $this->m);
		$this->lat = fmod($this->mb/$this->m,360);
		$this->rx = ' ';
    }


}

?>