<?php 
namespace App\Astrology;

use App\Astrology\Helpers\Convert;
use App\Astrology\Helpers\JulianDate;

class Houses{

    public $asc;
	public $mc;
	public $vx;
	public $ep;
	public $ci;
	public $ob;
	public $ra;
	public $lat;
	public $lng;
    public $gmt;
    
    public function __construct($day, $month, $year, $gmt, $lat, $lng)
    {
        $jd = new JulianDate($month, $day, $year, $gmt);
		$this->lat = deg2rad($lat);
		$this->lng = deg2rad($lng);
		$this->ci = $jd->ci;
		$this->gmt = $gmt;
		$this->ob = deg2rad(23.452294 - .0130125 * $this->ci);
		$this->ra = deg2rad(Convert::Mod360((6.6460656 + 2400.0513 * $this->ci + 2.58e-5 * $this->ci * $this->ci + $gmt) * 15 - $lng));
		$this->asc = $this->asc();
		$this->mc  = $this->mc();
		$this->vx = $this->vx();
        $this->ep = $this->ep();
        
        $this->PlacHouses();
    }

    private function asc()
	{
		$b=atan(cos($this->ra) / (-sin($this->ra) * cos($this->ob) - tan($this->lat) * sin($this->ob)));
		if ($b < 0.0)
			$b += M_PI;
		if (cos($this->ra) < 0.0)
			$b += M_PI;
		return (Convert::Mod360(rad2deg($b)));
    }

	private function mc()
	{
		$m = atan(tan($this->ra) / cos($this->ob));
		if ($m < 0.0)
			$m += M_PI;
		if ($this->ra > M_PI)
			$m += M_PI;
		return (Convert::Mod360(rad2deg($m)));
    }

	private function vx()
	{
		$x = cos($this->ra + M_PI);

		$y = -sin($this->ra + M_PI) * cos($this->ob) - sin($this->ob) / tan($this->lat);
		$v = atan($x / $y);
		if ($v < 0.0)
			$v += M_PI;
		if ($x < 0.0)
			$v += M_PI;
		return (Convert::Mod360(rad2deg($v)));
	}

	private function ep()
	{
		if ($this->ra)
			$e = atan(cos($this->ra) / (-sin($this->ra) * cos($this->ob)));
		else
			$e = 0;
			
		if ($e < 0.0)
			$e += M_PI;
		if (cos($this->ra) < 0.0)
			$e += M_PI;
		return Convert::Mod360(rad2deg($e));
	}
	
	private function PlacHouses()
	{
		$this->house[0] = $this->asc;
		$this->house[6] = Convert::Mod360($this->house[0] + 180);

		$this->house[9] = $this->mc;		
		$this->house[3] = Convert::Mod360($this->house[9] + 180);

		$this->house[1] = $this->plac_house(2);
		$this->house[7] = Convert::Mod360($this->house[1] + 180);
		
		$this->house[2] = $this->plac_house(3);
		$this->house[8] = Convert::Mod360($this->house[2] + 180);
		
		$this->house[10] = $this->plac_house(11);
		$this->house[4] = Convert::Mod360($this->house[10] + 180);
		
		$this->house[11] = $this->plac_house(12);
		$this->house[5] = Convert::Mod360($this->house[11] + 180);
	}

	private function plac_house($h)
	{
		switch($h)
		{
			case 2:
				$ff = 1.5;
				$k = 1;
				break;
			case 3:
				$ff = 3.0;
				$k = 1;
				break;
			case 11:
				$ff = 3.0;
				$k = -1;
				break;
			case 12:
				$ff = 1.5;
				$k = -1;
				break;
		}
		
		$r1 = $this->ra;
		for ($i=1;$i<=10;$i++)
		{
			$xx=acos($k * sin($r1) * tan($this->ob) * tan($this->lat));
			if ($xx < 0)
				$xx+=M_PI;
			$r2=$this->ra+($xx/$ff);
			if ($k==1)
				$r2 = $this->ra + M_PI - ($xx/$ff);
			$r1=$r2;
		}
		$lo = atan(tan($r1)/cos($this->ob));
		if ($lo < 0)
			$lo+=M_PI;
		if (sin($r1)<0)
			$lo+=M_PI;
		return (Convert::Mod360(rad2deg($lo)));
	}
}

?>