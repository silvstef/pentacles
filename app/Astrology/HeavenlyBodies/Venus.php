<?php 

namespace App\Astrology\HeavenlyBodies;

use App\Astrology\Interfaces\HeavenlyBody;
use App\Astrology\Planet;
use App\Astrology\HeavenlyBodies\Sun;
use App\Astrology\Helpers\JulianDate;

class Venus extends Planet implements HeavenlyBody
{
    private $mean_anomaly = array(212.6032 , 58517.8039 , .0013);
    private $eccentricy = array(.00682 ,-.00005 , 0.0);
    private $astronimical_unit = .7233;
    private $perihelion_argument = array( 54.3842, .5082 ,-.0014);
    private $ascending_node = array( 75.7796, .8999, .0004);
    private $inclination = array( 3.3936, .001 , 0.0);
    private $harmonics = null;
    private $no_term = null;
    public $index = 2;
    public $name = "Venus";
    public $sun;
    private $julianDate;

    public function __construct(Sun $sun, $d, $m, $y, $gmt)
    {
        $this->name = "Venus";
        $this->sun = $sun;
        $this->julianDate = new JulianDate($m, $d, $y, $gmt);
        parent::__construct($this->sun, $this->mean_anomaly, $this->eccentricy, $this->perihelion_argument, $this->ascending_node, $this->inclination, $this->harmonics, $this->no_term, $this->astronimical_unit, $this->julianDate);

        $this->initialCalculations();

		$this->calculatePointsOfOrigin();

		$this->calculateHarmonics();

		$this->calculateOrbit();
    }
}

?>