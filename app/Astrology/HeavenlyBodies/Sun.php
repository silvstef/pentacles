<?php 

namespace App\Astrology\HeavenlyBodies;

use App\Astrology\Interfaces\HeavenlyBody;
use App\Astrology\Planet;
use App\Astrology\Helpers\JulianDate;

class Sun extends Planet implements HeavenlyBody
{
    private $mean_anomaly = array(358.47584, 35999.0498, -.00015);
    private $eccentricy = array(.016751, -.000041, 0.0);
    private $astronimical_unit = 1.00000013;
    private $perihelion_argument = array(101.22083, 1.71918, .00045);
    private $ascending_node = array(0.0, 0.0, 0.0);
    private $inclination = array(0.0, 0.0, 0.00);
	private $harmonics = null;
	private $no_term = null;
    public $index = 0;
    public $name = "Sun";
    private $julianDate;
    public function __construct($d, $m, $y, $gmt)
    {
        $this->name = "Sun";
        $this->julianDate = new JulianDate($m, $d, $y, $gmt); //careful when swapping $month and $day .. JulianDate accepts (american) $month, $day, $year ...
        parent::__construct(null, $this->mean_anomaly, $this->eccentricy, $this->perihelion_argument, $this->ascending_node, $this->inclination, $this->harmonics, $this->no_term, $this->astronimical_unit, $this->julianDate);
		
		$this->initialCalculations();

		$this->calculatePointsOfOrigin();

		$this->calculateHarmonics();

		$this->calculateOrbit();
    }
}

?>