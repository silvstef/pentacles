<?php 

namespace App\Astrology\HeavenlyBodies;

use App\Astrology\Interfaces\HeavenlyBody;
use App\Astrology\Planet;
use App\Astrology\HeavenlyBodies\Sun;
use App\Astrology\Helpers\JulianDate;

class Neptune extends Planet implements HeavenlyBody
{
    private $mean_anomaly = array(30.13294, 240.45516, 0.0);
    private $eccentricy = array(.00913, -.00127 , 0.0);
    private $astronimical_unit = 30.11375;
    private $perihelion_argument = array( 284.1683, -21.6329, 0.0);
    private $ascending_node = array( 130.68415, 1.1005, 0.0);
    private $inclination = array( 1.7794,-.0098, 0.0);
    private $harmonics = array ( .1832, -.6718, .2726, -.1923, 175.7, 31.8, .0122, 542.1, 189.6, .0027, 1219.4, 
                            178.1, -.00496, 3035.6, -31.3, -.1122, .166, -.0544, -.00496, 3035.3, 58.7, .0961, 177.1, -68.8,
                            -.0073, 630.9, 51.0, -.0025, 1236.6, 78.0, .00196, -.0119, .0111, .0001, 3049.3, 44.2, -.0002, 
                            893.9, 48.5, .00007, 1416.5, -.252);
    private $no_term = 4;
    public $index = 7;
    public $name = "Neptune";
    public $sun;
    private $julianDate;

    public function __construct(Sun $sun, $d, $m, $y, $gmt)
    {
        $this->name = "Neptune";
        $this->sun = $sun;
        $this->julianDate = new JulianDate($m, $d, $y, $gmt);
        parent::__construct($this->sun, $this->mean_anomaly, $this->eccentricy, $this->perihelion_argument, $this->ascending_node, $this->inclination, $this->harmonics, $this->no_term, $this->astronimical_unit, $this->julianDate);
        
        $this->initialCalculations();

		$this->calculatePointsOfOrigin();

		$this->calculateHarmonics();

		$this->calculateOrbit();
    }
}

?>