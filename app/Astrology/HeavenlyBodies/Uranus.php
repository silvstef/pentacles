<?php 

namespace App\Astrology\HeavenlyBodies;

use App\Astrology\Interfaces\HeavenlyBody;
use App\Astrology\Planet;
use App\Astrology\HeavenlyBodies\Sun;
use App\Astrology\Helpers\JulianDate;

class Uranus extends Planet implements HeavenlyBody
{
    private $mean_anomaly = array(74.1757 , 427.2742, 0.0);
    private $eccentricy = array(.04682 ,.00042 , 0.0);
    private $astronimical_unit = 19.2215;
    private $perihelion_argument = array( 95.6863, 2.0508, 0.0);
    private $ascending_node = array( 73.5222, .5242, 0.0);
    private $inclination = array( .7726, .0001, 0.0);
    private $harmonics = array ( -.0021, -.0159, 0, .0299, 422.3, -17.7, -.0049, 3035.1, -31.3,
                            -.0038, 945.3, 60.1, -.0023, 1227, -4.99, .0134, -.02186, 0.0, .0317,
                            404.3 ,81.9, -.00495, 3037.9, 57.3, .004, 993.5, -54.4, -.0018, 1249.4,
                            79.2, -.0003, .0005, 0.0, .0005, 352.5, -54.99, .0001, 3027.5, 54.2,
                            -.0001, 1150.3, -88);
    private $no_term = 4;
    public $index = 6;
    public $name = "Uranus";
    public $sun;
    private $julianDate;
    public function __construct(Sun $sun, $d, $m, $y, $gmt)
    {
        $this->name = "Uranus";
        $this->sun = $sun;
        $this->julianDate = new JulianDate($m, $d, $y, $gmt);
        parent::__construct($this->sun, $this->mean_anomaly, $this->eccentricy, $this->perihelion_argument, $this->ascending_node, $this->inclination, $this->harmonics, $this->no_term, $this->astronimical_unit, $this->julianDate);
        
        $this->initialCalculations();

		$this->calculatePointsOfOrigin();

		$this->calculateHarmonics();

		$this->calculateOrbit();
    }
}

?>