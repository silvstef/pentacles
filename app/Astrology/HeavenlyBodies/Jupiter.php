<?php 

namespace App\Astrology\HeavenlyBodies;

use App\Astrology\Interfaces\HeavenlyBody;
use App\Astrology\Planet;
use App\Astrology\HeavenlyBodies\Sun;
use App\Astrology\Helpers\JulianDate;

class Jupiter extends Planet implements HeavenlyBody
{
    private $mean_anomaly = array(225.4928, 3033.6879, 0.0);
    private $eccentricy = array(.04838, .00002, 0.0);
    private $astronimical_unit = 5.2029;
    private $perihelion_argument = array(273.393, 1.3383, 0.0);
    private $ascending_node = array(99.4198, 1.0583, 0.0);
    private $inclination = array(1.3097, -.0052, 0.0);
    private $harmonics = array(-.001,-.0005,.0045,.0051,581.7,-9.7,-.0005,2510.7,-12.5,-.0026,1313.7,
                            -61.4,.0013,2370.79,-24.6,-.0013,3599.3,37.7,-.001,2574.7,31.4,-.00096,6708.2,-114.5,
                            -.0006,5499.4,-74.97,-.0013,1419,54.2,.0006,6339.3,-109,.0007,4824.5,-50.9,.0020,-.0134,
                            .0127,-.0023,676.2,.9,.00045,2361.4,174.9,.0015,1427.5,-188.8,.0006,2110.1,153.6,.0014,3606.8,
                            -57.7,-.0017,2540.2,121.7,-.00099,6704.8,-22.3,-.0006,5480.2,24.5,.00096,1651.3,-118.3,.0006,
                            6310.8,-4.8,.0007,4826.6,36.2);
    private $no_term = 11;
    public $index = 4;
    public $name = "Jupiter";
    public $sun;
    private $julianDate;

    public function __construct(Sun $sun, $d, $m, $y, $gmt)
    {
        $this->name = "Jupiter";
        $this->sun = $sun;
        $this->julianDate = new JulianDate($m, $d, $y, $gmt);
        parent::__construct($this->sun, $this->mean_anomaly, $this->eccentricy, $this->perihelion_argument, $this->ascending_node, $this->inclination, $this->harmonics, $this->no_term, $this->astronimical_unit, $this->julianDate);
        
        $this->initialCalculations();

		$this->calculatePointsOfOrigin();

		$this->calculateHarmonics();

		$this->calculateOrbit();
    }
}

?>