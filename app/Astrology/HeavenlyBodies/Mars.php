<?php 

namespace App\Astrology\HeavenlyBodies;

use App\Astrology\Interfaces\HeavenlyBody;
use App\Astrology\Planet;
use App\Astrology\HeavenlyBodies\Sun;
use App\Astrology\Helpers\JulianDate;

class Mars extends Planet implements HeavenlyBody
{
    private $mean_anomaly = array(319.5293, 19139.8585, .00018);
    private $eccentricy = array(.09331, .00009, 0.0);
    private $astronimical_unit = 1.52369;
    private $perihelion_argument = array(285.43176, 1.06977, .00013);
    private $ascending_node = array(48.78644, .77099, 0.0);
    private $inclination = array(1.85033, -.00068, .0001);
    private $harmonics = null;
    private $no_term= null;
    public $index = 3;
    public $name = "Mars";
    public $sun;
    private $julianDate;

    public function __construct(Sun $sun, $d, $m, $y, $gmt)
    {
        $this->name = "Mars";
        $this->sun = $sun;
        $this->julianDate = new JulianDate($m, $d, $y, $gmt);
        parent::__construct($this->sun, $this->mean_anomaly, $this->eccentricy, $this->perihelion_argument, $this->ascending_node, $this->inclination, $this->harmonics, $this->no_term, $this->astronimical_unit, $this->julianDate);
        
        $this->initialCalculations();

		$this->calculatePointsOfOrigin();

		$this->calculateHarmonics();

		$this->calculateOrbit();
    }
}

?>