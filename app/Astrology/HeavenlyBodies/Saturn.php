<?php 

namespace App\Astrology\HeavenlyBodies;

use App\Astrology\Interfaces\HeavenlyBody;
use App\Astrology\Planet;
use App\Astrology\HeavenlyBodies\Sun;
use App\Astrology\Helpers\JulianDate;

class Saturn extends Planet implements HeavenlyBody
{
    private $mean_anomaly = array(174.2153, 1223.50796, 0);
    private $eccentricy = array(.05423, -.0002, 0.0);
    private $astronimical_unit = 9.5525;
    private $perihelion_argument = array(338.9117, -.3167, 0.0);
    private $ascending_node = array( 112.8261, .8259, 0.0);
    private $inclination = array(2.4908, -.0047, 0.0);
    private $harmonics = array(-.0009,.0037,0,.0134,1238.9,-16.4,-.00426,3040.9,-25.2,.0064,
                            1835.3,36.1,-.0153,610.8,-44.2,-.0015,2480.5,-69.4,-.0014,.0026,0,.0111,
                            1242.2,78.3,-.0045,3034.96,62.8,-.0066,1829.2,-51.5,-.0078,640.6,24.2,
                            -.0016,2363.4,-141.4,.0006,-.0002,0,-.0005,1251.1,43.7,.0005,622.8,13.7,
                            .0003,1824.7,-71.1,.0001,2997.1,78.2);
    private $no_term = 5;
    public $name = "Saturn";
    private $julianDate;
    public function __construct(Sun $sun, $d, $m, $y, $gmt)
    {
        $this->name = "Saturn";
        $this->sun = $sun;
        $this->julianDate = new JulianDate($m, $d, $y, $gmt);
        parent::__construct($this->sun, $this->mean_anomaly, $this->eccentricy, $this->perihelion_argument, $this->ascending_node, $this->inclination, $this->harmonics, $this->no_term, $this->astronimical_unit, $this->julianDate);
        
        $this->initialCalculations();

		$this->calculatePointsOfOrigin();

		$this->calculateHarmonics();

		$this->calculateOrbit();
    }
}

?>