<?php 

namespace App\Astrology\HeavenlyBodies;

use App\Astrology\Interfaces\HeavenlyBody;
use App\Astrology\Planet;
use App\Astrology\HeavenlyBodies\Sun;
use App\Astrology\Helpers\JulianDate;

class Pluto extends Planet implements HeavenlyBody
{
    private $mean_anomaly = array(229.781, 145.1781, 0.0);
    private $eccentricy = array(.24797, 0.002898, 0.0);
    private $astronimical_unit = 39.539;
    private $perihelion_argument = array( 113.5366, 0.2086, 0.0);
    private $ascending_node = array( 108.944, 1.3739, 0.0);
    private $inclination = array( 17.1514, -0.0161, 0.0);
    private $harmonics = array ( -.0426, .073, -.029, .0371, 372.0, -331.3, -.0049, 3049.6,
                            -39.2, -.0108, 566.2, 318.3, .0003, 1746.5, -238.3, -.0603, .5002, -0.6126,
                            .049, 273.97, 89.97, -.0049, 3030.6, 61.3, .0027, 1075.3, -28.1, -.0007,
                            1402.3, 20.3, .0145, -.0928, .1195, .0117, 302.6, -77.3, .00198, 528.1,
                            48.6, -.0002, 1000.4, -46.1);
    private $no_term = 4;
    public $index = 8;
    public $name = "Pluto";
    public $sun;
    private $julianDate;

    public function __construct(Sun $sun, $d, $m, $y, $gmt)
    {
        $this->name = "Pluto";
        $this->sun = $sun;
        $this->julianDate = new JulianDate($m, $d, $y, $gmt);
        parent::__construct($this->sun, $this->mean_anomaly, $this->eccentricy, $this->perihelion_argument, $this->ascending_node, $this->inclination, $this->harmonics, $this->no_term, $this->astronimical_unit, $this->julianDate);
        
        $this->initialCalculations();

		$this->calculatePointsOfOrigin();

		$this->calculateHarmonics();

		$this->calculateOrbit();
    }
}

?>