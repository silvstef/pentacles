<?php 

namespace App\Astrology\HeavenlyBodies;

use App\Astrology\Interfaces\HeavenlyBody;
use App\Astrology\Planet;
use App\Astrology\HeavenlyBodies\Sun;
use App\Astrology\Helpers\JulianDate;

class Mercury extends Planet implements HeavenlyBody
{
    private $mean_anomaly = array(102.27938 , 149472.515 , 0.0);
    private $eccentricy = array(.205614 , .00002 , 0.0);
    private $astronimical_unit = .387098;
    private $perihelion_argument = array( 28.75375 , .37028 , .00012);
    private $ascending_node = array( 47.14594 , 1.1852 , .00017);
    private $inclination = array( 7.00288 , .00186 ,-.00001);
    private $harmonics = null;
    private $no_term = null;
    public $index = 1;
    public $name = "Mercury";
    public $sun;
    private $julianDate;

    public function __construct(Sun $sun, $d, $m, $y, $gmt)
    {
        $this->name = "Mercury";
        $this->sun = $sun;
        $this->julianDate = new JulianDate($m, $d, $y, $gmt);
        parent::__construct($this->sun, $this->mean_anomaly, $this->eccentricy, $this->perihelion_argument, $this->ascending_node, $this->inclination, $this->harmonics, $this->no_term, $this->astronimical_unit, $this->julianDate);

        $this->initialCalculations();

		$this->calculatePointsOfOrigin();

		$this->calculateHarmonics();

		$this->calculateOrbit();
    }
}

?>