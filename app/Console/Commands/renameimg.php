<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class renameimg extends Command
{
    /**
     * The name and signature of the console command.
     * RENAMES ALL FILES FROM PUBLIC IMG TO PROFIELS/PROFILE_{INDEX}
     *
     * @var string
     */
    protected $signature = 'rename';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $directory = ('public/img');
        echo $directory."\n";
        $files = Storage::allFiles($directory);
        foreach($files as $k=>$file)
        {
            if(Storage::exists($file) && $file!="public/img/.DS_Store")
            {
                $fileName='proflie_'.($k+1).'.jpg';
                $file_obj = new File(storage_path('app/'.$file));
                Storage::putFileAs('public/pictures', $file_obj, $fileName);
                $this->info($fileName);
            }
        }
    }
}
