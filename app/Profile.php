<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Cache;

class Profile extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['id','user_id','created_at','updated_at','deleted_at','birthday','address','country','region','phone','phone_extra','email_extra'];
    protected $appends = ['slug','profile_picture_url','profile_picture_alt'];
    protected $casts = [
        'physical_attributes' => 'array'
    ];
    const GUEST_FEATURED_LIST_CACHE_KEY = 'guest_featured_list_cache_key_';
    const USER_FEATURED_LIST_CACHE_KEY = 'user_featured_list_cache_key_';
    const FEATURED_LIST_CACHE_TIME = 60 * 60 * 48;
    const PROFILE_PICTURE_CACHE_TIME = 60 * 60 * 24 * 7;
    const PROFILE_PICTURE_CACHE_KEY = 'profile_picture_cache_key_';
    public function images()
    {
        return $this->hasMany('App\Image');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function profile_picture()
    {
        return Cache::remember(self::PROFILE_PICTURE_CACHE_KEY.$this->id, self::PROFILE_PICTURE_CACHE_TIME, function(){
            if($this->profile_picture_id === null)
            {
                return Image::where('profile_id', $this->id)->first();
            }
            return Image::find($this->profile_picture_id);
        });
    }

    public function getSlugAttribute()
    {
        return $this->nickname;
    }

    public function getProfilePictureUrlAttribute()
    {
        return $this->profile_picture()->public_url;
    }

    public function getProfilePictureAltAttribute()
    {
        return $this->profile_picture()->alt;
    }

    public function getFeaturedList()
    {
        return Cache::remember(self::USER_FEATURED_LIST_CACHE_KEY, self::FEATURED_LIST_CACHE_TIME, function(){
            return $this->orderBy('updated_at','desc')->take(20)->get();
        });
    }

    public static function getGuestFeaturedList()
    {
        return Cache::remember(self::GUEST_FEATURED_LIST_CACHE_KEY, self::FEATURED_LIST_CACHE_TIME, function(){
            return Profile::orderBy('updated_at','desc')->take(20)->get();
        });
    }
}
