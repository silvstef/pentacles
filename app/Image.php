<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Cache;

class Image extends Model
{
    protected $guarded = ['id'];
    protected $appends = ['alt'];
    protected $fillable = ['profile_id', 'url', 'width', 'height', 'is_private'];
    const IMAGE_ALT_CACHE_KEY = 'image_alt_cache_key_';
    const IMAGE_ALT_CACHE_TIME = 60 * 60 * 24 * 7;
    public function profile()
    {
        return $this->belongsTo('App\Profile');
    }

    static public function default_url()
    {
        return asset('storage/default_profile.png');
    }
    
    public function getPublicUrlAttribute()
    {
        return asset($this->url);
    }

    public function getAltAttribute()
    {
        return Cache::remember(self::IMAGE_ALT_CACHE_KEY, self::IMAGE_ALT_CACHE_TIME, function(){
            return $this->profile->nickname . ' from ' . config('app.name');
        });
    }

}
