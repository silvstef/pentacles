<?php

namespace App\Http\Controllers;

use App\UsersOffer;
use Illuminate\Http\Request;

class UsersOfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UsersOffer  $usersOffer
     * @return \Illuminate\Http\Response
     */
    public function show(UsersOffer $usersOffer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UsersOffer  $usersOffer
     * @return \Illuminate\Http\Response
     */
    public function edit(UsersOffer $usersOffer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UsersOffer  $usersOffer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UsersOffer $usersOffer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UsersOffer  $usersOffer
     * @return \Illuminate\Http\Response
     */
    public function destroy(UsersOffer $usersOffer)
    {
        //
    }
}
