<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Astrology\Sign;
use App\Astrology\Chart;
use App\Astrology\House;

class HomeController extends Controller
{
    public function index()
    {
        return view('home');
    }
}
