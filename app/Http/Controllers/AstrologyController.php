<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Astrology\Sign;
use App\Astrology\Chart;
use App\Astrology\House;

class AstrologyController extends Controller
{
    public function index()
    {
        return view('astrology.index');
    }

    public function submit(Request $request)
    {
        $this->validate($request, [

        ]);
        $day = $request->day;
        $month = $request->month;
        $year = $request->year;
        $gmt = $request->timezone;
        $lat = null;
        $long = null;
        $hour = null;
        $minute = null;
        if(!$request->unkown_time)
        {
            $hour = $request->hour;
            $minute = $request->minute;
            $gmt = $this->calculateJulianGmt($hour, $minute, $gmt);
            $lat = $request->lat;
            $long = $request->lng;
        }

        $chart = new Chart($day, $month, $year, $gmt, $lat, $long, $hour, $minute);

        return $chart->toArray();
    }

    public function tyrus()
    {
        $day = 27;
        $month = 11;
        $year = 1995;
        $timezone = -6;

        $hour = 6; //9
        $minute = 0;
        
        $long = -90.46119949999999;
        $lat = 30.50435829999999;
        for($i = 0; $i <= 15; $i++)
        {
            $gmt = $this->calculateJulianGmt($hour, $minute, $timezone);
            $chart = new Chart($day, $month, $year, $gmt, $lat, $long, $hour, $minute);
            echo sprintf("If born at %s:%s he would be %s ascendent",$hour,$minute,$chart->toArray()['extra']['ascendent']['name']);
            echo "\n<br>";
            foreach($chart->toArray()['houses'] as $k=>$house)
            {
                echo sprintf("%s house in %s <br>", $house['house_no'], $house['sign']['name']);
            }
            echo "\n<br>";
            $minute+=15;
            if($minute == 60)
            {
                $minute = 59;
            }
            if($minute>60)
            {
                $minute = 0;
                $hour++;
            }
        }
        return true;
    }

    private function calculateJulianGmt($hour, $minute, $timezone, $ampm = false)
    {
        $local_time = $hour + $minute/60;
        if ($ampm == 'pm')
        {
            $local_time += 12;
        }
        return $local_time + $timezone;
    }
}
