<?php

namespace App\Http\Controllers;

use App\UsersRole;
use Illuminate\Http\Request;

class UsersRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UsersRole  $usersRole
     * @return \Illuminate\Http\Response
     */
    public function show(UsersRole $usersRole)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UsersRole  $usersRole
     * @return \Illuminate\Http\Response
     */
    public function edit(UsersRole $usersRole)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UsersRole  $usersRole
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UsersRole $usersRole)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UsersRole  $usersRole
     * @return \Illuminate\Http\Response
     */
    public function destroy(UsersRole $usersRole)
    {
        //
    }
}
