<?php

namespace App\View\Components;

use Illuminate\View\Component;
use \App\Profile;
use \App\Settings;

class Footer extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */

    public function __construct()
    {
        
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        $profiles = Profile::orderBy('updated_at','desc')->take(10)->get();
        $settings = Settings::getFooter();
        $address = $settings['address'];
        $email = $settings['email'];
        $phone = $settings['phone'];
        $logo = "Astroweb";
        return view('components.footer',compact('profiles','address','email','phone','logo'));
    }
}
