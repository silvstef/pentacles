<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Menu extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        // $logo = asset('storage/logo.png');
        $logo = "Astroweb";
        $logo_alt = "Astroweb Logo";
        return view('components.menu',compact('logo','logo_alt'));
    }
}
