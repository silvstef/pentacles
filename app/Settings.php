<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    
    static public function getFooter()
    {
        //return Settings::orderBy('updated_at','desc')->first();
        return config('bomberfly.settings');
    }
}
