require('./bootstrap');

window.Vue = require('vue');
import VueP5 from 'vue-p5';

Vue.component('birthchart-calculator', require('./components/BirthchartCalculator.vue').default);
Vue.component('birthchart', require('./components/Birthchart.vue').default);
Vue.component('quickchart', require('./components/Quickchart.vue').default);
Vue.component('register-user', require('./components/RegisterUser.vue').default);

const app = new Vue({
    el: '#app',
});