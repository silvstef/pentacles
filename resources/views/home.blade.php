@extends('layouts.app')

@section('content')
<div id="app">
    <birthchart-calculator api_key="{{env('GOOGLE_GEO_API_KEY')}}"></birthchart-calculator>
    
    @if(auth()->check())
        <my-birthcharts></my-birthcharts>
    @endif
    
</div>
@endsection