@php 
    $show_type = true;
    if(isset($_GET['type']))
    {
        $show_type = false;
        if(!in_array($_GET["type"],["hostess","client"]))
        {
            $show_type = true;
        }
    }
@endphp
                        
@extends('layouts.app')

@section('content')
    <register-user :data="{{json_encode($data)}}"></register-user>
<div class="py-10 w-full bg-gray-100">
    <div class="m-auto w-5/12 border-b border-orange-600 py-2 px-2">
        <h4 class="text-lg font-semibold text-gray-700">Register</h4>
    </div>
    <div class="m-auto w-5/12 bg-white shadow border rounded border-gray-400 mt-4">
        <form action="" class="py-2 px-4 flex flex-col">
            @csrf

            <div class="flex items-center mt-2">
                <div class="w-1/3">
                    <label for="#nickname" class="text-gray-700 block text-right mr-3">Nickname</label>
                </div>
                <div class="w-2/3">
                    <input type="text" name="nickname" id="nickname" value="{{ old('nickname') }}" required autofocusclass="bg-white border rounded px-4 py-2 w-full border-gray-500">
                </div>
            </div>
            <div class="flex items-center mt-2">
                <div class="w-1/3">
                    <label for="#email" class="text-gray-700 block text-right mr-3">E-Mail</label>
                </div>
                <div class="w-2/3">
                    <input type="email" name="email" id="email" value="{{ old('email') }}" required class="bg-white border rounded px-4 py-2 w-full border-gray-500">
                </div>
            </div>
            <div class="flex items-center mt-2">
                <div class="w-1/3">
                    <label for="#password" class="text-gray-700 block text-right mr-3">Password</label>
                </div>
                <div class="w-2/3">
                    <input type="password" name="password" id="password" class="bg-white border rounded px-4 py-2 w-full border-gray-500">
                </div>
            </div>
            @if(false)
            @if($show_type)
            <div>
                <input type="radio" name="customer_type">
                <label>I'm looking for girls</label>
            </div>
            <div>
                <input type="radio" name="customer_type">
                <label>I'm looking for clients</label>
            </div>
            @else
                <input type="hidden" name="customer_type" value="{{$_GET['type']}}" />
            @endif
            @endif
            <div class="mt-4 mb-2 w-full text-md">
                <a class="w-full text-center inline-block px-4 py-2 leading-none border rounded text-white bg-orange-400 border-orange-400 hover:text-white hover:bg-orange-600 hover:border-orange-600 mt-4 lg:mt-0 md:mt-0">Register</a>
            </div>
            <div class="mt-2 border-none">
                <div class="border-t-2 border-gray-600 flex items-center justify-between px-3">
                    <a class="text-white bg-blue-500 text-center border rounded px-4 py-2 mt-2 mb-2 w-5/12 whitespace-no-wrap"><i class="fa fa-facebook" aria-hidden="true"></i> | Facebook</a>
                    <a class="text-white bg-red-600 text-center border rounded px-4 py-2 mt-2 mb-2 w-5/12 whitespace-no-wrap"><i class="fa fa-google" aria-hidden="true"></i> | Google</a>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $("#show_password_field").on("click", function(){
        var $eye = $(this).find('.fa');
        if($eye.hasClass('fa-eye'))
        {
            $("#password").removeAttr('type').attr('type','text');
        }
        else
        {
            $("#password").removeAttr('type').attr('type','password');
        }
        $eye.toggleClass('fa-eye fa-eye-slash');
    });
</script>
@endsection
