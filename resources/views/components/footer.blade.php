<footer class="w-full px-6 py-4 text-white bg-silver-300 flex justify-between border-gold-600 border-t-2">
    <div class="pl-10 text-gold-900 text-sm">
        <h3 class="text-gold-500 text-2xl">{{$logo}}</h3>
        <p>Birthchart</p>
        <p>Meet</p>
        <p>Youtube Channel</p>
        <p>Facebook Page</p>
        <p>Blog</p>
        <p>About</p>
    </div>
    <div class="flex flex-col text-black text-sm">
        <p class="text-gold-800 text-lg">Daily</p>
        <p>Planetary Positions</p>
        <p>Moon Calendar</p>
        <p>Calendar of choice</p>
    </div>
    <div class="flex flex-col text-black text-sm">
        <p class="text-gold-800 text-lg">For devs</p>
        <p>Calculator Repo</p>
        <p>Dev Blog</p>
    </div>
    <div class="flex flex-col text-black text-sm pr-10">
        <p class="text-gold-800 text-lg">Contacts</p>
        <p><span>Phone: </span> {{$phone}}</p>
        <p><span>E-Mail: </span> {{$email}}</p>
        <p>{{$address}}</p>
        <p class="mt-1"><a href="{{route('contact_us')}}" class="px-2 py-1 border rounded bg-transparent border-black hover:border-gold-500 hover:text-gold-500">Contact us</a></p>
    </div>
</footer>