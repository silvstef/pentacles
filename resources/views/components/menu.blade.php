<nav class="flex items-center justify-between flex-wrap p-6 bg-gray-900 border-b-2 border-gold-500">
  <div class="pl-10">
    <h3 class="text-gold-400 text-3xl">{{$logo}}</h3>
    {{-- <img src="{{$logo}}" alt="{{$logo_alt}}" class="p-1" width="140" height="auto"> --}}
  </div>
  <div class="block lg:hidden md:hidden">
    <button class="flex items-center px-3 py-2 border rounded text-teal-200 border-teal-400 hover:text-white hover:border-white">
      <svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
    </button>
  </div>
  <div class="w-full block justify-between flex-grow pr-10 ml-6 lg:flex lg:items-center lg:w-auto md:flex md:items-center md:w-auto">
    <div class="text-sm lg:flex-grow md:flex-grow">
      <a href="{{route('meet')}}" class="block mt-4 lg:inline-block lg:mt-0 md:mt-0 text-silver-400 hover:text-gold-600 mr-4">Meet</a>
      <a href="{{route('about')}}" class="block mt-4 lg:inline-block lg:mt-0 md:mt-0 text-silver-400 hover:text-gold-600 mr-4">About</a>
      <a href="{{route('contact_us')}}" class="block mt-4 lg:inline-block lg:mt-0 md:mt-0 text-silver-400 hover:text-gold-600 mr-4">Contact Us</a>
    </div>
    <div class="text-sm">
      <input type="text" placeholder="Blue eyes, Scorpio, Blonde" class="inline-block text-sm text-gold-400 px-4 py-2 mr-12 leading-none border border-gold-600 rounded bg-transparent ">
      <a href="{{route('login')}}" class="inline-block text-sm px-4 py-2 leading-none border rounded text-silver-400 bg-transparent border-gold-400 hover:border-gold-600 hover:text-gold-600 mt-4 lg:mt-0 md:mt-0">Login</a>
      <a href="{{route('register')}}" class="inline-block text-sm px-4 py-2 leading-none border rounded text-black bg-gold-400 border-gold-500 hover:text-silver-500 hover:bg-gold-600 hover:border-gold-600 mt-4 lg:mt-0 md:mt-0">Register</a>
    </div>
  </div>
</nav>