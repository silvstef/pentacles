<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/contact', 'HomeController@contact_us')->name('contact_us');
Route::get('/meet', 'HomeController@meet')->name('meet');
Route::get('/about', 'HomeController@about')->name('about');

Route::get('/astrology', 'AstrologyController@index')->name('astrology_index');
Route::get('/tyrus', 'AstrologyController@tyrus');
Route::post('/astrology', 'AstrologyController@submit')->name('submit_chart');

Route::get('/p/{slug}', 'ProfileController@publicShow')->name('profile');

Route::group(['middleware'=>'auth'], function(){
    Route::resources([
        'profile' => 'ProfileController',
        'image'   => 'ImageController',
        'comment' => 'CommentController',
        'post'    => 'PostController'
    ]);
});
Route::group(['middleware'=>'admin.auth'], function(){
    Route::resources([
        'settings'=>'SettingsController',
        'role'=>'UsersRoleController',
    ]);
});

Auth::routes();