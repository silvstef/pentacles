<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProfileSensitiveInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->date("birthday")->nullable();
            $table->string("address")->nullable();
            $table->string("country")->nullable();
            $table->string("region")->nullable();
            $table->string("city")->nullable();
            $table->string("phone")->nullable();
            $table->string("phone_extra")->nullable();
            $table->string("email_extra")->nullable();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->dropColumn("birthday");
            $table->dropColumn("address");
            $table->dropColumn("country");
            $table->dropColumn("region");
            $table->dropColumn("city");
            $table->dropColumn("phone");
            $table->dropColumn("phone_extra");
            $table->dropColumn("email_extra");
        });
    }
}
