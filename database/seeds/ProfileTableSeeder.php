  
<?php

use Illuminate\Database\Seeder;
use App\Profile;
use App\Image;
use App\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class ProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    public function run()
    {
      //blabalbal
      $directory = ('public/pictures');
      echo $directory."\n";
      $files = Storage::allFiles($directory);
      foreach($files as $k=>$file)
      {
          if(Storage::exists($file) && $file!="public/pictures/.DS_Store")
          {
            $file = str_replace("public/pictures/","storage/pictures/",$file);
            $image = factory(App\Image::class)->make(['fileName'=>$file]);
            $user = factory(User::class)->create();
            //dd($user->id);
            $user->profile()
            ->save(
              factory(App\Profile::class)->make(
                ['profile_picture_id'=>$image->id,
                'user_id'=>$user->id]
              )
            );
            
            $image->create(['profile_id'=>$user->profile->id,'url'=>($file),'width'=>'auto','height'=>'auto','is_private'=>0]);
          }
      }


    }
}