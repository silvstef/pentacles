<?php

use Illuminate\Database\Seeder;

class ImageTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        

        $this->enableForeignKeys();
    }
}
