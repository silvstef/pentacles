<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Image;
use Faker\Generator as Faker;

$factory->define(Image::class, function (Faker $faker) {
    return [
        'width' => 'auto',
        'height' => 'auto'
    ];
});
