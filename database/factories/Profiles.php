<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Profile;
use Faker\Generator as Faker;
use \Carbon\Carbon;

$factory->define(Profile::class, function (Faker $faker) {
    $p_a_json = array(
        'eyes'=>$faker->randomElement(array('Blue','Brown','Green','Black', 'Dark Green', 'Gray', 'Light Brown','Light Blue','Dark Brown','Light Green','Gray Green')),
        'hair'=>$faker->randomElement(array('Blonde','Red','Brown','Dark','Black','Dark Brown','Light Brown','Paints ocasionally','White')),

    );
    return [
        'about' => $faker->realText((int)rand(100,300),(int)rand(1,4)),
        'nickname' => $faker->firstNameFemale,
        'level' => $faker->randomElement(array('New', 'Rising Star', 'Queen', 'Princess')),
        'rating' => rand(3.5,5),
        'birthday'=> Carbon::now()->subYear((int)rand(20,32))->subMonths((int)rand(0,11))->subDays((int)rand(0,31)),
        'address'=> $faker->address,
        'country'=> $faker->country,
        'region'=> $faker->state,
        'city'=> $faker->city,
        'phone'=> $faker->phoneNumber,
        'physical_attributes'=> json_encode($p_a_json),
    ];
});